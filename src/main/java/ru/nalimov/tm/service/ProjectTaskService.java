package ru.nalimov.tm.service;

import ru.nalimov.tm.entity.Project;
import ru.nalimov.tm.entity.Task;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.exceptions.TaskNotFoundException;
import ru.nalimov.tm.repository.ProjectRepository;
import ru.nalimov.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private static final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private static final TaskRepository taskRepository = TaskRepository.getInstance();

    private static ProjectTaskService instance = null;

    public ProjectTaskService() {
    }

    public static ProjectTaskService getInstance() {
        synchronized (ProjectTaskService.class) {
            return instance == null
                    ? instance = new ProjectTaskService()
                    : instance;
        }
    }

    public List<Task> findAddByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAddByProjectId(projectId);
    }

    public Task addTaskToProject(final Long projectId, final Long taskId) throws TaskNotFoundException, ProjectNotFoundException {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public Task removeTaskFromProject (final Long projectId, final Long taskId) throws TaskNotFoundException {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
