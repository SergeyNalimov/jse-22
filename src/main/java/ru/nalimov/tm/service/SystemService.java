package ru.nalimov.tm.service;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

public class SystemService {

    private int HISTORY_SIZE = 10;
    private static SystemService instance = null;
    private final Deque<String> commandHistory = new ArrayDeque<>(HISTORY_SIZE);

    private SystemService() {
    }

    public static SystemService getInstance() {
        synchronized (SystemService.class) {
            return instance == null
                    ? instance = new SystemService()
                    : instance;
        }

    }


    public int addCommandToHistory(final String command) {
        if (commandHistory.size() == HISTORY_SIZE)
            commandHistory.removeFirst();
        commandHistory.addLast(command);
        return 0;
    }


    public int displayVersion() {
        System.out.println("ver 1.0.18");
        return 0;
    }

    public int displayAbout() {
        System.out.println("nalimov_sv@nlmk.com");
        return 0;
    }

    public int displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println();
        System.out.println("project-create - Create new project by name");
        System.out.println("project-list - Display list of projects");
        System.out.println("project-clear - Remove all project");
        System.out.println("project-view-by-index - display project by index");
        System.out.println("project-view-by-id - display project by id");
        System.out.println("project-remove-by-name - delete project by name");
        System.out.println("project-remove-by-id - delete project by id");
        System.out.println("project-remove-by-index - delete project by index");
        System.out.println("project-update-by-index - update project by index");
        System.out.println("project-update-by-id - update project by id");
        System.out.println();
        System.out.println("task-create - create new task by name");
        System.out.println("task-list - display list of tasks");
        System.out.println("task-clear - remove all task");
        System.out.println("task-view-by-index - display project by index");
        System.out.println("task-view-by-id - display project by id");
        System.out.println("task-remove-by-name - delete task by name");
        System.out.println("task-remove-by-id - delete task by id");
        System.out.println("task-remove-by-index - delete task by index");
        System.out.println("task-update-by-index - update task by index");
        System.out.println("task-update-by-id - update project by id");
        System.out.println();
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids");
        System.out.println();
        System.out.println("user-create");
        System.out.println("admin-create");
        System.out.println("users-clear");
        System.out.println("users-list");
        System.out.println("user-view-by-login");
        System.out.println("user-remove-by-login");
        System.out.println("user-update-by-login");
        System.out.println("user-view-by-id");
        System.out.println("user-remove-by-id");
        System.out.println("user-update-by-id");
        System.out.println("user-authentic");
        System.out.println("user-deauthentic");
        System.out.println("user-change-password");
        System.out.println("user-update-role");
        System.out.println("user-profile-update");
        System.out.println("user-profile");
        System.out.println("task-add-to-user-by-ids");
        System.out.println("task-clear-by-user-id");
        System.out.println("task-list-by-user-id");
        System.out.println("project-add-to-user-by-ids");
        System.out.println("project-clear-by-user-id");
        System.out.println("project-list-by-user-id");
        return 0;
    }

    public int displayHistory() {
        System.out.println("userlog");
        ArrayList<String> list = new ArrayList<String>(commandHistory);
        for (int i = 0; i < list.size(); i++) {
            System.out.println("Input №" + (i+1) + ": " + list.get(i));
        }
        return 0;
    }

    public int displayExit() {
        System.out.println("Exit...");
        return 0;
    }

    public void displayWelcome() {
        System.out.println("Welcome");
    }

    public int displayErr() {
        System.out.println("Error! Unknow key...");
        return -1;
    }

    public int displayForAdminOnly() {
        System.out.println("THIS FUNCTIONALITY IS FOR ADMINS ONLY! FAIL.");
        return -1;
    }

}
