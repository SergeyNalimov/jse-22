package ru.nalimov.tm.service;

import java.util.Scanner;

public class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

    protected String printError() {
        System.out.println("Error! Please try again!");
        final String command = scanner.nextLine();
        return command;
    }

}
