package ru.nalimov.tm.entity;

import java.io.Serializable;
import java.util.Comparator;

public class Task implements Serializable {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    private Long projectId;

    private Long userId = System.nanoTime();

    public Task() {}

    public Task(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getUserId() { return userId; }

    public void setUserId(final Long userId) {
        this.userId = userId;
    }

    public Task(Long id, String name, String description, Long projectId, Long userId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", projectId=" + projectId +
                ", userId=" + userId +
                '}';
    }

    public static Comparator<Task> TaskSortByName = new Comparator<Task>() {
    @Override
        public int compare(Task t1, Task t2) {
            return t1.getName().compareTo(t2.getName());
        }
    };

}
